FROM library/nginx:latest

RUN mkdir -p /data
ADD ./ /data/
WORKDIR /data
COPY nginx.conf /etc/nginx/nginx.conf
RUN chmod -R 777 /data/dist

EXPOSE 80 80
